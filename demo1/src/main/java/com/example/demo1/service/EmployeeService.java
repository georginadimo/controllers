package com.example.demo1.service;

import com.example.demo1.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAllEmployees();
}
