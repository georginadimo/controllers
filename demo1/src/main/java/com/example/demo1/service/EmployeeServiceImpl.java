package com.example.demo1.service;

import com.example.demo1.model.Employee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private static List<Employee> employees;
    static {
        employees = populateDummyUsers();
    }
    private static List<Employee> populateDummyUsers(){
        List<Employee> employees= new ArrayList<>();
        employees.add(new Employee(1,"geo",12,987));
        employees.add(new Employee(1,"geo",12,987));
        return employees;
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employees;
    }
}
