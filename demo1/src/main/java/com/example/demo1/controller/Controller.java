package com.example.demo1.controller;

import com.example.demo1.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import com.example.demo1.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j

public class Controller {
    @Autowired
    EmployeeService employeeService;
    @RequestMapping(value = "/testme",method = RequestMethod.GET)
    public ResponseEntity<String> testme() {
        String myreturn="hello";
        return new ResponseEntity<String>(myreturn, HttpStatus.OK);
    }

    @RequestMapping(value = "/employee",method = RequestMethod.GET)
    public  ResponseEntity<List<Employee>> listAllUsers(){
        log.info("your request arrived");
        List<Employee> employees =  employeeService.findAllEmployees();
        if(employees.isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);

        }
        return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
    }
}
